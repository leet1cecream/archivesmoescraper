import requests
import sqlite3
import logging
import yaml
import time
from concurrent.futures import ThreadPoolExecutor

# LOGGING SETUP ==================================================================
logging.basicConfig()
logger = logging.getLogger("ArchivedMoeScraper")
logger.setLevel(logging.INFO)

# LOADING CONFIG ==================================================================
logger.info("Loading config")
with open("config.yaml", "r") as configFile:
    config = yaml.safe_load(configFile)

# GETTING DATA FROM DB ==================================================================
logger.info("Creating database engine")
db = sqlite3.connect(config["database"]["name"], check_same_thread=False)
cur = db.cursor()
selectEtagNullQuery = "select id, link from files where etag is null;"
cur.execute(selectEtagNullQuery)
data = cur.fetchall()
cur.close()


etags = []


def update(postId, link, p):
    pp = {
        'http': 'http://' + p,
        # 'https': 'https://' + p,
    }
    try:
        ret = requests.head(link, headers={"User-Agent": config["requests"]["userAgent"]},
                            proxies=pp, allow_redirects=True, timeout=3)
    except Exception as e:
        print(e)
        print("Failed to update ID {} | URL: {} | Proxy: {}".format(postId, link, p))
        return
    if not ret.ok or ret.status_code != 200:
        print("Failed to update ID {} | URL: {} | Code: {} | Proxy: {} | Headers: {}"
                       .format(postId, link, ret.status_code, p, str(ret.headers)[0:100]))
        return
    etag = ret.headers.get("etag")
    etags.append([postId, link, etag])
    print("Fetched ID: {} |  URL: {} | ETag: {}".format(postId, link, etag))
    # print("Got etag: {}".format(ret.headers.get("etag")))
    # return ret.headers.get("etag")


with open("proxies.txt", "r") as f:
    proxies = f.readlines()

while len(proxies) < 30000:
    proxies.append(proxies)
proxyIter = iter(proxies)

with ThreadPoolExecutor(max_workers=24) as executor:
    futures = [executor.submit(update, postId, link, next(proxyIter)) for postId, link in data]
    # proxyIter = iter(proxies)
    # for postId, link in data:
    #     future = executor.submit(update, postId, link, next(proxyIter))
    #     time.sleep(0.02)

cur.close()
