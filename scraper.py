import datetime
import logging
import os.path
import sqlite3
import re
import random
import string
import hashlib
from dataclasses import dataclass
from datetime import datetime
from time import sleep

import requests
import yaml
import lxml
from bs4 import BeautifulSoup

logging.basicConfig()
logger = logging.getLogger("ArchivedMoeScraper")
logger.setLevel(logging.INFO)

logger.info("Loading config")
with open("config.yaml", "r") as configFile:
    config = yaml.safe_load(configFile)

logger.info("Creating database engine")
db = sqlite3.connect(config["database"]["name"])


@dataclass
class Thread:
    postId: int
    title: str
    text: str
    link: str
    date: datetime


@dataclass
class File:
    postId: int
    text: str
    name: str
    original_name: str
    extension: str
    width: int
    height: int
    size: int
    link: str
    path: str
    fileHash: str
    date: datetime
    thread: Thread


def initDatabase():
    logger.info("Initializing database")

    cur = db.cursor()
    createThreadsQuery = """CREATE TABLE IF NOT EXISTS threads (
        id INT NOT NULL,
        title VARCHAR(256),
        text VARCHAR(2000),
        link VARCHAR(124),
        date DATETIME,
        CONSTRAINT primary_id PRIMARY KEY (id)
    );"""

    createFilesQuery = """CREATE TABLE IF NOT EXISTS files (
        id INT NOT NULL,
        text VARCHAR(2000),
        name VARCHAR(256),
        original_name VARCHAR(256),
        extension VARCHAR(32),
        width INT,
        height INT,
        size INT,
        link VARCHAR(256),
        path VARCHAR(512),
        file_hash VARCHAR(256),
        date DATETIME,
        CONSTRAINT primary_id PRIMARY KEY (id)
    );"""

    createThreadsFilesQuery = """CREATE TABLE IF NOT EXISTS threads_files (
        thread_id INT NOT NULL,
        file_id INT NOT NULL,
        CONSTRAINT primary_id PRIMARY KEY (thread_id, file_id),
        CONSTRAINT foreign_threads_files_thread FOREIGN KEY (thread_id) REFERENCES threads (id),
        CONSTRAINT foreign_threads_files_file FOREIGN KEY (file_id) REFERENCES files (id)
    );"""

    cur.execute(createThreadsQuery)
    cur.execute(createFilesQuery)
    cur.execute(createThreadsFilesQuery)

    db.commit()
    cur.close()


def saveThreadToDatabase(thread: Thread):
    cur = db.cursor()
    insertThreadQuery = "INSERT OR IGNORE INTO threads (id, title, text, link, date) VALUES (?, ?, ?, ?, ?);"
    cur.execute(insertThreadQuery, (thread.postId, thread.title, thread.text, thread.link, thread.date,))
    db.commit()
    cur.close()


def saveFileToDatabase(file: File):
    cur = db.cursor()
    insertThreadQuery = """INSERT OR IGNORE INTO files (id, text, name, original_name, extension, width, height, size, link, 
    path, file_hash, date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?);"""
    cur.execute(insertThreadQuery, (file.postId, file.text, file.name, file.original_name, file.extension, file.width,
                                    file.height, file.size, file.link, file.path, file.fileHash, file.date))
    db.commit()
    cur.close()


def saveThreadFileAssociation(thread: Thread, file: File):
    cur = db.cursor()
    insertAssocQuery = "INSERT OR IGNORE INTO threads_files (thread_id, file_id) VALUES (?, ?);"
    cur.execute(insertAssocQuery, (thread.postId, file.postId,))
    db.commit()
    cur.close()


def getThreadByPostId(postId: int):
    cur = db.cursor()
    selectThreadQuery = "SELECT * FROM threads WHERE id = ?;"
    cur.execute(selectThreadQuery, (postId,))
    thread = cur.fetchone()
    if thread is None:
        return None
    return Thread(postId=thread[0], title=thread[1], text=thread[2], link=thread[3], date=thread[4])


def getFileByPostId(postId: int):
    cur = db.cursor()
    selectFileQuery = "SELECT * FROM files WHERE id = ?;"
    cur.execute(selectFileQuery, (postId,))
    file = cur.fetchone()
    if file is None:
        return None
    return File(postId=file[0], text=file[1], name=file[2], original_name=file[3],
                extension=file[4], width=file[5], height=file[6], size=file[7],
                link=file[8], path=file[9], fileHash=file[10], date=file[11], thread=None)


def getFileByHash(fileHash: str):
    cur = db.cursor()
    selectFileQuery = "SELECT * FROM files WHERE file_hash = ?;"
    cur.execute(selectFileQuery, (fileHash,))
    file = cur.fetchone()
    if file is None:
        return None
    return File(postId=file[0], text=file[1], name=file[2], original_name=file[3],
                extension=file[4], width=file[5], height=file[6], size=file[7],
                link=file[8], path=file[9], fileHash=file[10], date=file[11], thread=None)


def threadExistsByPostId(postId: int):
    if getThreadByPostId(postId) is not None:
        return True
    return False


def fileExistsByPostId(postId: int):
    if getFileByPostId(postId) is not None:
        return True
    return False


def fileExistsByHash(fileHash: str):
    if getFileByHash(fileHash) is not None:
        return True
    return False


def getRequest(url: str):
    retries = 0
    while retries < config["requests"]["maxRetries"]:
        ret = None
        try:
            ret = requests.get(url, headers={"User-Agent": config["requests"]["userAgent"]},
                               timeout=config["requests"]["timeout"])
        except requests.RequestException:
            retries = retries + 1
            logger.warning("Retrying {}/{} | URL: {}".format(retries, config["requests"]["maxRetries"], url))
            sleep(1)
            continue

        if not ret.ok or ret.status_code != 200:
            retries = retries + 1
            logger.warning("Retrying {}/{} | URL: {} | Status: {} | Content: {}"
                           .format(retries, config["requests"]["maxRetries"],
                                   url, ret.status_code, ret.content[0:120]))
            sleep(1)
            continue
        return ret.content
    return ""


def getThreadsFromSearch(searchText: str, page: int):
    ret = getRequest(config["scraper"]["searchUrl"].format(config["scraper"]["board"],
                                                           config["scraper"]["searchText"], page))
    soup = BeautifulSoup(ret, "lxml")
    for br in soup.find_all("br"):
        br.replaceWith("\n")

    threads = []

    for post in soup.find_all("div", {"class": "post_wrapper"}):
        try:
            postId = post.find("a", {"data-function": "quote"}).text
            title = post.find("h2", {"class": "post_title"}).text
            text = post.find("div", {"class": "text"}).text
            link = config["scraper"]["threadUrl"].format(config["scraper"]["board"],
                                                         post.select("[data-post]")[0]["data-post"])
            date = datetime.fromisoformat(post.find("time")["datetime"])
            threads.append(Thread(postId=postId, title=title, text=text, link=link, date=date))
        except:
            logger.warning("Failed to process thread: {}".format(post))

    logger.info("Found threads: {} -> Search: {} | Page: {}".format(len(threads), searchText, page))
    return threads


def getFilesFromThread(thread: Thread):
    ret = getRequest(config["scraper"]["threadUrl"].format(config["scraper"]["board"], thread.postId))

    soup = BeautifulSoup(ret, "lxml")
    for br in soup.find_all("br"):
        br.replaceWith("\n")

    files = []

    for post in soup.find_all("article", class_=lambda value: value and "post" in value.split()
                                                              and "has_image" in value.split()):
        try:
            postId = post.find("a", {"data-function": "quote"}).text
            text = post.find("div", {"class": "text"}).text
            name = ""
            originalName = post.find("a", {"class": "post_file_filename"})
            if "title" in originalName:
                originalName = post.find("a", {"class": "post_file_filename"})["title"]
            else:
                originalName = post.find("a", {"class": "post_file_filename"}).text
            extension = originalName[originalName.rfind(".") + 1:]

            if extension == "webm" and config["scraper"]["ignoreWebms"]:
                continue

            metadata = post.find("span", {"class": "post_file_metadata"}).text
            resolution = re.search("^.*\\s(\\d*x\\d*).*$", metadata).group(1)
            width = re.search("^(\\d*)x.*$", resolution).group(1)
            height = re.search("^.*x(\\d*)$", resolution).group(1)
            size = 0
            link = post.find("a", {"class": "post_file_filename"})["href"]
            path = ""
            fileHash = ""
            date = datetime.fromisoformat(post.find("time")["datetime"])
            files.append(File(postId=postId, text=text, name=name, original_name=originalName, extension=extension,
                              width=width, height=height, size=size, link=link, path=path, fileHash=fileHash,
                              date=date, thread=thread))
        except Exception as e:
            logger.warning("Failed to proccess post: {} | Thread: {}".format(post, thread))

    logger.info("Found files: {} -> Thread ID: {}".format(len(files), thread.postId))
    return files


def getRandomFileName():
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=config["scraper"]["fileNameLength"]))


def saveFile(file: File):
    file.name = getRandomFileName() + "." + file.extension

    ret = getRequest(file.link)
    if not len(ret):
        logger.warning("Failed to save file -> Post ID: {} | URL: {}".format(file.postId, file.link))
        return False

    file.size = len(ret)
    file.path = "files/" + config["scraper"]["folderName"] + "/" + file.name
    file.fileHash = hashlib.sha256(ret).hexdigest()

    if fileExistsByHash(file.fileHash):
        persistedFile = getFileByHash(file.fileHash)
        saveThreadFileAssociation(file.thread, persistedFile)
        logger.info("Duplicate file found: {} | Hash: {} | Skipping...".format(persistedFile.postId,
                                                                                persistedFile.fileHash))
        return True

    with open(file.path, "wb") as fileHandler:
        fileHandler.write(ret)

    saveFileToDatabase(file)
    saveThreadFileAssociation(file.thread, file)
    logger.info("Saved file -> Post ID: {0} | Name: {1:64} | Hash: {2}".format(file.postId,
                                                                               file.original_name, file.fileHash))
    return True


def main():
    logger.info("Creating folders")
    if not os.path.exists("files"):
        os.mkdir("files")
    if not os.path.exists("files/" + config["scraper"]["folderName"]):
        os.mkdir("files/" + config["scraper"]["folderName"])

    for i in range(1, 5000):
        threads = getThreadsFromSearch(config["scraper"]["searchText"], i)
        for thread in threads:
            if threadExistsByPostId(thread.postId):
                logger.info("Duplicate thread found: {} | Skipping ...".format(thread.postId))
                continue
            files = getFilesFromThread(thread)
            for file in files:
                if fileExistsByPostId(file.postId):
                    logger.info("Duplicate file found: {} | Skipping...".format(file.postId,))
                    continue
                saveFile(file)
            saveThreadToDatabase(thread)


if __name__ == "__main__":
    initDatabase()
    main()
